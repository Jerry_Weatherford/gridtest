#pragma once
struct CState
{
	const CString m_strName;
	const CString m_strAbbreviation;
	const CString m_strCapital;
	const CString m_strLargestCity;
	const long m_lPopulation;
	const long m_lArea;
	const CString m_strTimeZone;
	const CString m_strTimeZone1;
	const bool m_bUsesDst;
public:
	CState(
		const CString& strName,
		const CString& strAbbreviation,
		const CString& strCapital,
		const CString& strLargestCity,
		long lPopulation,
		long lArea,
		const CString& strTimeZone,
		const CString& strTimeZone1,
		bool bUsesDst
		);
	~CState();
};

