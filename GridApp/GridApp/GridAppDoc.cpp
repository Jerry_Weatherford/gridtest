// GridAppDoc.cpp : implementation of the CGridAppDoc class
//

#include "stdafx.h"
#include "GridApp.h"

#include "GridAppDoc.h"
#include <vector>
#include <memory>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGridAppDoc

IMPLEMENT_DYNCREATE(CGridAppDoc, CDocument)

BEGIN_MESSAGE_MAP(CGridAppDoc, CDocument)
END_MESSAGE_MAP()


// CGridAppDoc construction/destruction

CGridAppDoc::CGridAppDoc()
{
}

CGridAppDoc::~CGridAppDoc()
{
}

const std::vector<CState> CGridAppDoc::GetStates()
{
	if (m_States.empty())
	{
		OnNewDocument();
	}

	return m_States;
}

BOOL CGridAppDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	const CString strDataFile = "States.csv";
	CFileStatus status;
	if (!CFile::GetStatus(strDataFile, status))
	{
		CString msg;
		CString curDir;
		GetCurrentDirectory(MAX_PATH, curDir.GetBufferSetLength(MAX_PATH));
		msg.Format("Data file %s does not exists in the current directory:\n\t%s", strDataFile, curDir);
		curDir.ReleaseBuffer();

		AfxMessageBox(msg);
		PostQuitMessage(0);
	}

	CCSVFile csvFile(strDataFile);

	CStringArray row;
	csvFile.ReadData(row);
	m_Titles.insert(m_Titles.begin(), &row[0], &row[row.GetSize() - 1] + 1);

	while (csvFile.ReadData(row))
	{
		auto strPopulation = row[4]; strPopulation.Replace(",", "");
		auto strArea = row[5]; strArea.Replace(",", "");
			
			CState state(
			row[0], //Name
			row[1], //Abbreviation
			row[2], //Capital
			row[3], //LargestCity
			atol(strPopulation), // Population
			atol(strArea), // Area
			row[6], // TimeZone
			row[7], // TimeZone1
			row[8]=="YES"  // UsesDst
			);
		m_States.push_back(state);
	}

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


// CGridAppDoc serialization

void CGridAppDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
	}
}


// CGridAppDoc diagnostics

#ifdef _DEBUG
void CGridAppDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGridAppDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}

#endif //_DEBUG


// CGridAppDoc commands
