// GridAppView.h : interface of the CGridAppView class
//


#pragma once


class CGridAppView : public CBCGPGridView
{
	const int COL_NAME = 0;
	const int COL_ABBR = 1;
	const int COL_CAP = 2;
	const int COL_CITY = 3;
	const int COL_POP = 4;
	const int COL_AREA = 5;
	const int COL_ZONE = 6;
	const int COL_ZONE1 = 7;
	const int COL_DST = 8;

protected: // create from serialization only
	CGridAppView();
	DECLARE_DYNCREATE(CGridAppView)

// Attributes
public:
	CGridAppDoc* GetDocument() const;

// Operations
public:

// Overrides
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CGridAppView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CBCGPGridCtrl* m_pWndGrid;
	const int IDC_GRDID = 1;

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg LRESULT OnPrintClient(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnChangeVisualManager(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void SetViewType(int nType, BOOL bByNavigationButtons);
	void AdjustLayout();
};

#ifndef _DEBUG  // debug version in GridAppView.cpp
inline CGridAppDoc* CGridAppView::GetDocument() const
   { return reinterpret_cast<CGridAppDoc*>(m_pDocument); }
#endif

