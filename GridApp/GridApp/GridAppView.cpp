// GridAppView.cpp : implementation of the CGridAppView class
//

#include "stdafx.h"
#include "GridApp.h"

#include "GridAppDoc.h"
#include "GridAppView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGridAppView

IMPLEMENT_DYNCREATE(CGridAppView, CBCGPGridView)

BEGIN_MESSAGE_MAP(CGridAppView, CBCGPGridView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CBCGPGridView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CBCGPGridView::OnFilePrint)
	ON_MESSAGE(WM_PRINTCLIENT, OnPrintClient)
	ON_REGISTERED_MESSAGE(BCGM_CHANGEVISUALMANAGER, OnChangeVisualManager)
	ON_WM_CREATE()
END_MESSAGE_MAP()

// CGridAppView construction/destruction

CGridAppView::CGridAppView()
{
	EnableActiveAccessibility();
	// TODO: add construction code here
}

CGridAppView::~CGridAppView()
{
}

BOOL CGridAppView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return __super::PreCreateWindow(cs);
}

std::vector<CBCGPGridItem*> GetRowItems(CBCGPGridRow* pRow)
{
	std::vector<CBCGPGridItem*> vectItems;
	for (int i = 0; i < pRow->GetItemCount(); i++)
		vectItems.push_back(pRow->GetItem(i));
	return vectItems;
}

int CGridAppView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (__super::OnCreate(lpCreateStruct) == -1)
		return -1;

	// construct the Grid object

	CRect rectClient;
	GetClientRect(rectClient);

	m_pWndGrid = GetGridCtrl();

	CGridAppDoc* pDoc = (CGridAppDoc*)GetDocument();

	//     // Create the Windows control and attach it to the Grid object
	// 	m_pWndGrid->Create(WS_CHILD | WS_VISIBLE | WS_BORDER, rectClient, this, IDC_GRDID);

	// Insert columns:

	const auto& states = pDoc->GetStates();

	for (auto title : pDoc->GetTitles())
		m_pWndGrid->InsertColumn(m_pWndGrid->GetColumnCount(), title, 100);

	const auto columnCount = m_pWndGrid->GetColumnCount();

	// Insert rows
	for (auto state : states)
	{
		// Create new row:
		CBCGPGridRow* pRow = m_pWndGrid->CreateRow(columnCount);
		int nCol = 0;
		pRow->GetItem(COL_NAME)->SetValue(CComVariant(state.m_strName));
		pRow->GetItem(COL_ABBR)->SetValue(CComVariant(state.m_strAbbreviation));
		pRow->GetItem(COL_CAP)->SetValue(CComVariant(state.m_strCapital));
		pRow->GetItem(COL_CITY)->SetValue(CComVariant(state.m_strLargestCity));
		pRow->GetItem(COL_POP)->SetValue(CComVariant(state.m_lPopulation));
		pRow->GetItem(COL_AREA)->SetValue(CComVariant(state.m_lArea));
		pRow->GetItem(COL_ZONE)->SetValue(CComVariant(state.m_strTimeZone));
		pRow->GetItem(COL_ZONE1)->SetValue(CComVariant(state.m_strTimeZone1));
		pRow->GetItem(COL_DST)->SetValue(CComVariant(state.m_bUsesDst));

		for (auto item : GetRowItems(pRow))
			item->AllowEdit(FALSE);

		// Add row to grid:
		m_pWndGrid->AddRow(pRow, FALSE /* Don't recal. layout */);
	}
	m_pWndGrid->AdjustLayout();

	m_pWndGrid->SetWindowPos(NULL, 0, 0, rectClient.Width(), rectClient.Height(),
		SWP_NOACTIVATE | SWP_NOZORDER);

	//SetViewType(((CMainFrame*)GetParent())->GetCurrentViewType());

	m_pWndGrid->EnableGroupByBox(TRUE);

	m_pWndGrid->InsertGroupColumn(0, COL_ZONE);

	m_pWndGrid->ShowColumnsChooser(TRUE);

	return 0;
}


void CGridAppView::SetViewType(int nType, BOOL bByNavigationButtons)
{
	m_pWndGrid->SetScrollBarsStyle(CBCGPScrollBar::BCGP_SBSTYLE_VISUAL_MANAGER);
	m_pWndGrid->ShowWindow(SW_SHOW);

	AdjustLayout();
	m_pWndGrid->SetFocus();

// 	if (bByNavigationButtons)
// 	{
// 		((CMainFrame*)GetParent())->UpdateCurrentView(nType);
// 	}
// 	else
// 	{
// 		m_arNavigationHistory.Add(nType);
// 		m_nNavHistoryIndex = (int)m_arNavigationHistory.GetSize() - 1;
// 	}

	//UpdateZoomSlider();
}

void CGridAppView::AdjustLayout()
{
	if (m_pWndGrid->GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	m_pWndGrid->SetWindowPos(NULL, 0, 0, rectClient.Width(), rectClient.Height(),
		SWP_NOACTIVATE | SWP_NOZORDER);
}


// CGridAppView drawing

void CGridAppView::OnDraw(CDC* /*pDC*/)
{
	CGridAppDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
}


// CGridAppView printing

void CGridAppView::OnFilePrintPreview()
{
	BCGPPrintPreview (this);
}

BOOL CGridAppView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CGridAppView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CGridAppView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CGridAppView diagnostics

#ifdef _DEBUG
void CGridAppView::AssertValid() const
{
	__super::AssertValid();
}

void CGridAppView::Dump(CDumpContext& dc) const
{
	__super::Dump(dc);
}

CGridAppDoc* CGridAppView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGridAppDoc)));
	return (CGridAppDoc*)m_pDocument;
}
#endif //_DEBUG


// CGridAppView message handlers

LRESULT CGridAppView::OnPrintClient(WPARAM wp, LPARAM lp)
{
	if ((lp & PRF_CLIENT) == PRF_CLIENT)
	{
		CDC* pDC = CDC::FromHandle((HDC) wp);
		ASSERT_VALID(pDC);
		
		OnDraw(pDC);
	}
	
	return 0;
}

LRESULT CGridAppView::OnChangeVisualManager(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	return 0;
}
