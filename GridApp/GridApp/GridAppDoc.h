// GridAppDoc.h : interface of the CGridAppDoc class
//


#pragma once

#include "CSVFile.h"
#include <vector>
#include "State.h"

class CGridAppDoc : public CDocument
{
protected: // create from serialization only
	CGridAppDoc();
	DECLARE_DYNCREATE(CGridAppDoc)

	// Attributes
public:

	// Operations
public:

	// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

	// Implementation
public:
	virtual ~CGridAppDoc();
	const std::vector<CState>GetStates();
	const std::vector<CString>& GetTitles() const { return m_Titles; }

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	std::vector<CState>m_States;
	std::vector<CString> m_Titles;

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


