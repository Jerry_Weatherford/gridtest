// GridApp.h : main header file for the GridApp application
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols


// CGridAppApp:
// See GridApp.cpp for the implementation of this class
//

class CGridAppApp : public CBCGPWinApp
{
public:
	CGridAppApp();

	// Override from CBCGPWorkspace
	virtual void PreLoadState ();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CGridAppApp theApp;