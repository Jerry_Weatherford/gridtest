#include "stdafx.h"
#include "State.h"

CState::CState(
	const CString& strName, const CString& strAbbreviation, const CString& strCapital, 
	const CString& strLargestCity, long lPopulation, long lArea, 
	const CString& strTimeZone, const CString& strTimeZone1, bool bUsesDst) :
m_strName(strName),
m_strAbbreviation(strAbbreviation),
m_strCapital(strCapital),
m_strLargestCity(strLargestCity),
m_lPopulation(lPopulation),
m_lArea(lArea),
m_strTimeZone(strTimeZone),
m_strTimeZone1(strTimeZone1),
m_bUsesDst(bUsesDst)
{
}

CState::~CState()
{
}
