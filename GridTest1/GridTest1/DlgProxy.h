
// DlgProxy.h: header file
//

#pragma once

class CGridTest1Dlg;


// CGridTest1DlgAutoProxy command target

class CGridTest1DlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CGridTest1DlgAutoProxy)

	CGridTest1DlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CGridTest1Dlg* m_pDialog;

// Operations
public:

// Overrides
	public:
	virtual void OnFinalRelease();

// Implementation
protected:
	virtual ~CGridTest1DlgAutoProxy();

	// Generated message map functions

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CGridTest1DlgAutoProxy)

	// Generated OLE dispatch map functions

	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
	BSTR SayHello();

	enum
	{
		dispidSayHello = 1L
	};
};

