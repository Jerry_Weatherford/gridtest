

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Sun May 24 22:50:45 2015
 */
/* Compiler settings for GridTest1.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __GridTest1_h_h__
#define __GridTest1_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IGridTest1_FWD_DEFINED__
#define __IGridTest1_FWD_DEFINED__
typedef interface IGridTest1 IGridTest1;

#endif 	/* __IGridTest1_FWD_DEFINED__ */


#ifndef __GridTest1_FWD_DEFINED__
#define __GridTest1_FWD_DEFINED__

#ifdef __cplusplus
typedef class GridTest1 GridTest1;
#else
typedef struct GridTest1 GridTest1;
#endif /* __cplusplus */

#endif 	/* __GridTest1_FWD_DEFINED__ */


#ifndef __IDualAClick_FWD_DEFINED__
#define __IDualAClick_FWD_DEFINED__
typedef interface IDualAClick IDualAClick;

#endif 	/* __IDualAClick_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __GridTest1_LIBRARY_DEFINED__
#define __GridTest1_LIBRARY_DEFINED__

/* library GridTest1 */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_GridTest1;

#ifndef __IGridTest1_DISPINTERFACE_DEFINED__
#define __IGridTest1_DISPINTERFACE_DEFINED__

/* dispinterface IGridTest1 */
/* [uuid] */ 


EXTERN_C const IID DIID_IGridTest1;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("94FF16D6-CC84-4250-B4F2-8FE43BBE8652")
    IGridTest1 : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct IGridTest1Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGridTest1 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGridTest1 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGridTest1 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGridTest1 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGridTest1 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGridTest1 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGridTest1 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IGridTest1Vtbl;

    interface IGridTest1
    {
        CONST_VTBL struct IGridTest1Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGridTest1_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IGridTest1_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IGridTest1_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IGridTest1_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IGridTest1_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IGridTest1_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IGridTest1_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* __IGridTest1_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_GridTest1;

#ifdef __cplusplus

class DECLSPEC_UUID("B2F4678C-FD18-4570-AF54-63C4C571615B")
GridTest1;
#endif
#endif /* __GridTest1_LIBRARY_DEFINED__ */

#ifndef __IDualAClick_INTERFACE_DEFINED__
#define __IDualAClick_INTERFACE_DEFINED__

/* interface IDualAClick */
/* [object][dual][oleautomation][uuid] */ 


EXTERN_C const IID IID_IDualAClick;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0BDD0E81-0DD7-11cf-BBA8-5F34C9D5139C")
    IDualAClick : public IDispatch
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IDualAClickVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDualAClick * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDualAClick * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDualAClick * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDualAClick * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDualAClick * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDualAClick * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDualAClick * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IDualAClickVtbl;

    interface IDualAClick
    {
        CONST_VTBL struct IDualAClickVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDualAClick_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IDualAClick_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IDualAClick_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IDualAClick_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IDualAClick_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IDualAClick_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IDualAClick_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDualAClick_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


