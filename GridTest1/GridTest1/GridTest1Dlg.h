
// GridTest1Dlg.h : header file
//

#pragma once

#include <BCGCBProInc.h>			// BCGPro Control Bar

class CGridTest1DlgAutoProxy;


// CGridTest1Dlg dialog
class CGridTest1Dlg : public CDialogEx
{
	DECLARE_DYNAMIC(CGridTest1Dlg);
	friend class CGridTest1DlgAutoProxy;

// Construction
public:
	CGridTest1Dlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CGridTest1Dlg();

// Dialog Data
	enum { IDD = IDD_GRIDTEST1_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	CGridTest1DlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;
	CStatic m_wndGridLocation;
	CBCGPGridCtrl m_wndGrid;

	BOOL CanExit();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	DECLARE_MESSAGE_MAP()
};
