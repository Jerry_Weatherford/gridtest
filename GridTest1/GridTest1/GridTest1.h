
// GridTest1.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CGridTest1App:
// See GridTest1.cpp for the implementation of this class
//

class CGridTest1App : public CWinApp
{
public:
	CGridTest1App();

// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CGridTest1App theApp;