
// DlgProxy.cpp : implementation file
//

#include "stdafx.h"
#include "GridTest1.h"
#include "DlgProxy.h"
#include "GridTest1Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGridTest1DlgAutoProxy

IMPLEMENT_DYNCREATE(CGridTest1DlgAutoProxy, CCmdTarget)

CGridTest1DlgAutoProxy::CGridTest1DlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT_VALID(AfxGetApp()->m_pMainWnd);
	if (AfxGetApp()->m_pMainWnd)
	{
		ASSERT_KINDOF(CGridTest1Dlg, AfxGetApp()->m_pMainWnd);
		if (AfxGetApp()->m_pMainWnd->IsKindOf(RUNTIME_CLASS(CGridTest1Dlg)))
		{
			m_pDialog = reinterpret_cast<CGridTest1Dlg*>(AfxGetApp()->m_pMainWnd);
			m_pDialog->m_pAutoProxy = this;
		}
	}
}

CGridTest1DlgAutoProxy::~CGridTest1DlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CGridTest1DlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CGridTest1DlgAutoProxy, CCmdTarget)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CGridTest1DlgAutoProxy, CCmdTarget)
	DISP_FUNCTION_ID(CGridTest1DlgAutoProxy, "SayHello", dispidSayHello, SayHello, VT_BSTR, VTS_NONE)
END_DISPATCH_MAP()

// Note: we add support for IID_IGridTest1 to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .IDL file.

// {94FF16D6-CC84-4250-B4F2-8FE43BBE8652}
static const IID IID_IGridTest1 =
{ 0x94FF16D6, 0xCC84, 0x4250, { 0xB4, 0xF2, 0x8F, 0xE4, 0x3B, 0xBE, 0x86, 0x52 } };

BEGIN_INTERFACE_MAP(CGridTest1DlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CGridTest1DlgAutoProxy, IID_IGridTest1, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {B2F4678C-FD18-4570-AF54-63C4C571615B}
IMPLEMENT_OLECREATE2(CGridTest1DlgAutoProxy, "GridTest1.Application", 0xb2f4678c, 0xfd18, 0x4570, 0xaf, 0x54, 0x63, 0xc4, 0xc5, 0x71, 0x61, 0x5b)


// CGridTest1DlgAutoProxy message handlers


BSTR CGridTest1DlgAutoProxy::SayHello()
{
	AFX_MANAGE_STATE(AfxGetAppModuleState());

	CString strResult("Hello");

	return strResult.AllocSysString();
}
